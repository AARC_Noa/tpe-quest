extends Spatial


# Native
onready var ovr_api_proxy = preload("res://addons/godot_ovrmobile/OvrVrApiProxy.gdns").new()

# Script
onready var ovr_types = preload("res://addons/godot_ovrmobile/OvrVrApiTypes.gd").new()

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if (VrSystem.headset_type != VrSystem.HEADSET_TYPE.QUEST1 && 
		VrSystem.headset_type != VrSystem.HEADSET_TYPE.QUEST2):
		$"2D/SysPropViewport/SysPropDisplay".text = "Not Quest"
		return
		
		
	var sys_prop_display = ""
	for sys_prop in ovr_types.OvrSystemProperty:
		var sys_prop_id = ovr_types.OvrSystemProperty[sys_prop]
		var val = ovr_api_proxy.vrapi_get_system_property_int(sys_prop_id)
		
		sys_prop_display += str(sys_prop) + "(" + str(sys_prop_id) + ")"+ ":" + str(val) + "\n"
		
	$"2D/SysPropViewport/SysPropDisplay".text = sys_prop_display
	
	


func _on_Timer_timeout():
	var controller = randi() % 2 + 1
	var force = rand_range(.0, 1.0)
	
	$VR/VrPlayer.set_vibration(controller, force)
