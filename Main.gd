extends Spatial


onready var logLabel = $LogViewport/LogLabel
onready var stateLabel = $StateViewport/StateLabel
onready var vrPlayer = $VrPlayer

# Called when the node enters the scene tree for the first time.
func _ready():
	pass


func _on_VrPlayer_button_pressed(controller, button):
	logLabel.text = logLabel.text.insert(0, "button_pressed: (" + str(controller) + ", " + str(button) + ")\n")

func _on_VrPlayer_button_released(controller, button):
	logLabel.text = logLabel.text.insert(0, "button_released: (" + str(controller) + ", " + str(button) + ")\n")

func _on_VrPlayer_touch_pressed(controller, touch):
	logLabel.text = logLabel.text.insert(0, "touch_pressed: (" + str(controller) + ", " + str(touch) + ")\n")

func _on_VrPlayer_touch_released(controller, touch):
	logLabel.text = logLabel.text.insert(0, "touch_released: (" + str(controller) + ", " + str(touch) + ")\n")

func _process(delta):
	updateStateLabel()

func updateStateLabel():
	var txt = ""
	
	# left buttons
	txt += "left_button: "
	var lb = vrPlayer.get_buttons(1)
	for button in lb:
		txt += str(button) + ":" + str("1" if lb[button] else "0") + " / "
	txt += "\n"
	
	txt += "left_touch: "
	var lt = vrPlayer.get_touches(1)
	for button in lt:
		txt += str(button) + ":" + str("1" if lt[button] else "0") + " / "
	txt += "\n"
	
	# left joystick
	var leftAxis = vrPlayer.get_joystick_axis(1)
	txt += "left axis: X = " + str(leftAxis.x) + " ; Y = " + str(leftAxis.y) + "\n"
	
	# left analog buttons
	var leftAnalogs = vrPlayer.get_analog_buttons(1)
	txt += "left analogs: trig = " + str(leftAnalogs[vrPlayer.BUTTON_TRIGGER]) + " ; "
	txt += "grip = " + str(leftAnalogs[vrPlayer.BUTTON_GRIP]) + "\n"
	
	var mesh = vrPlayer.get_controller_raw(1).get_mesh()
	txt += "left mesh exists: " + "true" if mesh != null else "false\n"
	
	# right buttons
	txt += "right_button: "
	var rb = vrPlayer.get_buttons(2)
	for button in rb:
		txt += str(button) + ":" + str("1" if rb[button] else "0") + " / "
	txt += "\n"
	
	txt += "right_touch: "
	var rt = vrPlayer.get_touches(2)
	for button in rt:
		txt += str(button) + ":" + str("1" if rt[button] else "0") + " / "
	txt += "\n"
	
	# right joystick
	var rightAxis = vrPlayer.get_joystick_axis(2)
	txt += "right axis: X = " + str(rightAxis.x) + " ; Y = " + str(rightAxis.y) + "\n"
	
	# right analog buttons
	var rightAnalogs = vrPlayer.get_analog_buttons(2)
	txt += "right analogs: trig = " + str(rightAnalogs[vrPlayer.BUTTON_TRIGGER]) + " ; "
	txt += "grip = " + str(rightAnalogs[vrPlayer.BUTTON_GRIP]) + "\n"

	var rmesh = vrPlayer.get_controller_raw(1).get_mesh()
	txt += "right mesh exists: " + "true" if rmesh != null else "false\n"
	
	stateLabel.text = txt
