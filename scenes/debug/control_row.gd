extends Control


onready var touched:CheckBox = $Touched
onready var pressed:CheckBox = $Pressed
onready var analog:ProgressBar = $AnalogValue
onready var stick:Sprite = $StickBG/StickDot

const stickRange = 32.0

func set_pressed(v:bool):
	pressed.pressed = v
	
func set_touched(v:bool):
	touched.pressed = v
	
func set_analog(v:float):
	analog.value = v
	
func set_stick(v:Vector2):
	stick.position = v * stickRange
