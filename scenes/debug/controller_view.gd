extends Control

export(NodePath) var vr_player
export(int) var controller_id
export(String) var header

onready var _vr_player:VrPlayer = get_node(vr_player)



func _ready():
	$ControllerName.text = header
	
func _process(delta):
	var touches = _vr_player.get_touches(controller_id)
	var buttons = _vr_player.get_buttons(controller_id)
	var analogs = _vr_player.get_analog_buttons(controller_id)
	var stick = _vr_player.get_joystick_axis(controller_id)
	stick.y = -stick.y
	
	$ButtonInputs/A.set_pressed(buttons[VrPlayer.BUTTON_AX])
	$ButtonInputs/B.set_pressed(buttons[VrPlayer.BUTTON_BY])
	$ButtonInputs/Grip.set_pressed(buttons[VrPlayer.BUTTON_GRIP])
	$ButtonInputs/Trigger.set_pressed(buttons[VrPlayer.BUTTON_TRIGGER])
	$ButtonInputs/Menu.set_pressed(buttons[VrPlayer.BUTTON_MENU])
	$ButtonInputs/Stick.set_pressed(buttons[VrPlayer.BUTTON_STICK])
	
	$ButtonInputs/A.set_touched(touches[VrPlayer.BUTTON_AX])
	$ButtonInputs/B.set_touched(touches[VrPlayer.BUTTON_BY])
	$ButtonInputs/Trigger.set_touched(touches[VrPlayer.BUTTON_TRIGGER])
	
	$ButtonInputs/Trigger.set_analog(analogs[VrPlayer.BUTTON_TRIGGER])
	$ButtonInputs/Grip.set_analog(analogs[VrPlayer.BUTTON_GRIP])
	
	$ButtonInputs/Stick.set_stick(stick)
	
