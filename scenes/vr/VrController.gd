extends ARVRController

signal vr_button_pressed(controller, button)
signal vr_button_released(controller, button)

# Called when the node enters the scene tree for the first time.
func _ready():
	connect("button_pressed", self, "_on_button_pressed")
	connect("button_release", self, "_on_button_released")


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _on_button_pressed(button:int) -> void:
	emit_signal("vr_button_pressed", controller_id, button)
	
func _on_button_released(button:int) -> void:
	emit_signal("vr_button_released", controller_id, button)
