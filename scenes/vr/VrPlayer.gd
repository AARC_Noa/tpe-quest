extends ARVROrigin

class_name VrPlayer

# EXPORTS

export(NodePath) var left_controller_target
export(NodePath) var right_controller_target

export(float, 0, 0.5) var stick_deadzone = 0.3
export(bool) var circular_deadzone = true

export(float, 0, 0.5) var analog_button_deadzone = 0.1


# SIGNALS

signal button_pressed(controller, button)
signal button_released(controller, button)

signal touch_pressed(controller, touch)
signal touch_released(controller, touch)

# CONTROLLER OBJECTS
onready var lc = $LeftHand
onready var rc = $RightHand

onready var _lc_target:Spatial = get_node(left_controller_target)
onready var _rc_target:Spatial = get_node(right_controller_target)

# OVR BUTTON CONSTANTS

const OVR_BUTTON_AX = 7
const OVR_BUTTON_BY = 1
const OVR_BUTTON_STICK = 14
const OVR_BUTTON_TRIGGER = 15
const OVR_BUTTON_GRIP = 2
const OVR_BUTTON_MENU = 3

const OVR_TOUCH_TRIGGER = 11
const OVR_TOUCH_AX = 5
const OVR_TOUCH_BY = 6

const ovr_buttons = [OVR_BUTTON_AX, OVR_BUTTON_BY, OVR_BUTTON_STICK, OVR_BUTTON_TRIGGER, OVR_BUTTON_GRIP, OVR_BUTTON_MENU]
const ovr_touches = [OVR_TOUCH_TRIGGER, OVR_TOUCH_AX, OVR_TOUCH_BY]

# TPEQ BUTTON CONSTANTS

const BUTTON_AX = 1
const BUTTON_BY = 2
const BUTTON_STICK = 3
const BUTTON_TRIGGER = 4
const BUTTON_GRIP = 5
const BUTTON_MENU = 6

const buttons = [BUTTON_AX, BUTTON_BY, BUTTON_STICK, BUTTON_TRIGGER, BUTTON_GRIP, BUTTON_MENU]
const touches = [BUTTON_AX, BUTTON_BY,BUTTON_TRIGGER]

# MAPS

const button_map = {
	OVR_BUTTON_AX: BUTTON_AX,
	OVR_BUTTON_BY: BUTTON_BY,
	OVR_BUTTON_STICK: BUTTON_STICK,
	OVR_BUTTON_TRIGGER: BUTTON_TRIGGER,
	OVR_BUTTON_GRIP: BUTTON_GRIP,
	OVR_BUTTON_MENU: BUTTON_MENU
}

const touch_map = {
	OVR_TOUCH_AX: BUTTON_AX,
	OVR_TOUCH_BY: BUTTON_BY,
	OVR_TOUCH_TRIGGER: BUTTON_TRIGGER
}

# STATE

var _left_buttons = {}
var _left_touches = {}
var _left_vibration = .0

var _right_buttons = {}
var _right_touches = {}
var _right_vibration = .0

# FUNCS

func _ready():
	# Connect children
	
	lc.connect("vr_button_pressed", self, "_con_button_pressed")
	lc.connect("vr_button_released", self, "_con_button_released")
	
	rc.connect("vr_button_pressed", self, "_con_button_pressed")
	rc.connect("vr_button_released", self, "_con_button_released")
	
	# Prepare state dicts
	for button in buttons:
		_left_buttons[button] = false
		_right_buttons[button] = false
		
	for touch in touches:
		_left_touches[touch] = false
		_right_touches[touch] = false

func _process(_delta):
	var pSmooth = 0.8
	
	if _lc_target != null:
		_lc_target.global_transform = _lc_target.global_transform.interpolate_with(lc.global_transform, pSmooth)
		
	if _rc_target != null:
		_rc_target.global_transform = _rc_target.global_transform.interpolate_with(rc.global_transform, pSmooth)
	pass

# CONNECTIONS

func _con_button_pressed(controller: int, button: int) -> void: 
	if ovr_buttons.has(button):
		var dic = _left_buttons if controller == 1 else _right_buttons
		dic[button_map[button]] = true
		emit_signal("button_pressed", controller, button_map[button])
	elif ovr_touches.has(button):
		var dic = _left_touches if controller == 1 else _right_touches
		dic[touch_map[button]] = true
		emit_signal("touch_pressed", controller, touch_map[button])
		
func _con_button_released(controller:int, button:int) -> void:
	if ovr_buttons.has(button):
		var dic = _left_buttons if controller == 1 else _right_buttons
		dic[button_map[button]] = false
		emit_signal("button_released", controller, button_map[button])
	elif ovr_touches.has(button):
		var dic = _left_touches if controller == 1 else _right_touches
		dic[touch_map[button]] = false
		emit_signal("touch_released", controller, touch_map[button])

# PROPERTIES

var left_vibration = .0 setget _set_lc_rumble, _get_lc_rumble
var right_vibration = .0 setget _set_rc_rumble, _get_rc_rumble

# OBJ FUNCS

func get_buttons(controller:int) -> Dictionary:
	match controller:
		1: return _left_buttons.duplicate()
		2: return _right_buttons.duplicate()
	return {}
	
func get_touches(controller:int) -> Dictionary:
	match controller:
		1: return _left_touches.duplicate()
		2: return _right_touches.duplicate()
	return {}

func get_joystick_axis(controller:int) -> Vector2:
	var axes:Vector2 = Vector2(0,0)
	
	match controller:
		1: axes = Vector2( lc.get_joystick_axis(0), lc.get_joystick_axis(1) )
		2: axes = Vector2( rc.get_joystick_axis(0), rc.get_joystick_axis(1) )	
	
	if circular_deadzone:
		var length = axes.length()
		var norm = axes.normalized() * clamp(range_lerp(length, stick_deadzone, 1, 0, 1), 0, 1)
		
		return norm
	else: # Axial deadzone.
		return Vector2(
			_deadzone_lerp(axes.x, stick_deadzone, 1, 0, 1), 
			_deadzone_lerp(axes.y, stick_deadzone, 1, 0, 1)
		)

func get_analog_buttons(controller:int) -> Dictionary:
	match controller:
		1: return {
			BUTTON_TRIGGER: _remap_axis(lc.get_joystick_axis(2)), 
			BUTTON_GRIP: _remap_axis(lc.get_joystick_axis(3))
		}
		2: return {
			BUTTON_TRIGGER: _remap_axis(rc.get_joystick_axis(2)), 
			BUTTON_GRIP: _remap_axis(rc.get_joystick_axis(3))
		}
	return {}

func get_controller_raw(controller:int) -> ARVRController:
	match controller:
		1: return lc
		2: return rc
	return null
	
func get_vibration(controller:int) -> float:
	match controller:
		1: return _left_vibration
		2: return _right_vibration
	return .0
	
func set_vibration(controller:int, value:float) -> void:
	match controller:
		1: 
			_left_vibration = value
			lc.rumble = _left_vibration
		2: 
			_right_vibration = value
			rc.rumble = _right_vibration

# PRIV STUFF

func _remap_axis(ovr:float) -> float:	
	match VrSystem.headset_type:
		VrSystem.HEADSET_TYPE.QUEST1: 
				return clamp(range_lerp(range_lerp(ovr, -1, 1, 0, 1), analog_button_deadzone, 1, 0, 1), 0, 1) # [-1, 1] -> [0, 1] ; deadzone compensation ; back to [0, 1]
		VrSystem.HEADSET_TYPE.QUEST2: 
				return clamp(range_lerp(range_lerp(ovr, -1, 1, 0, 1), analog_button_deadzone, 1, 0, 1), 0, 1) # [-1, 1] -> [0, 1] ; deadzone compensation ; back to [0, 1]
		VrSystem.HEADSET_TYPE.RIFT:   return clamp(range_lerp(ovr,  analog_button_deadzone, 1, 0, 1), 0, 1) # [ 0, 1]
	return .0

func _deadzone_lerp(orig:float, istart:float, istop:float, ostart:float, ostop:float) -> float:
	if orig > 0:
		return clamp(range_lerp(orig, istart, istop, ostart, ostop), 0, 1)
	else:
		return -clamp(range_lerp(-orig, istart, istop, ostart, ostop), 0, 1)


# GETTERS AND SETTERS FOR VIBRATION - ANIMATION PURPOSES
func _set_lc_rumble(value:float): set_vibration(1, value)
func _set_rc_rumble(value:float): set_vibration(2, value)
func _get_lc_rumble(): get_vibration(1)
func _get_rc_rumble(): get_vibration(2)
