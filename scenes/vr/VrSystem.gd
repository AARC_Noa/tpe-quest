extends Node

enum HEADSET_TYPE { NONE, RIFT, QUEST1, QUEST2 }
var headset_type = HEADSET_TYPE.NONE
var perform_runtime_config = false
var dev_type

# Rift specific settings
var rift_config
var refresh_rate = 0

# Quest specific settings
var ovr_init_config
var ovr_performance


func _ready():
	var interface:ARVRInterface
	
	# Attempt loading for Rift
	if OS.get_name() == "Windows":
		var ovr_rift = preload("res://addons/godot-oculus/oculus_config.gdns")	
		rift_config = ovr_rift.new()
		
		interface = ARVRServer.find_interface("Oculus")
		if interface and interface.initialize():
			get_viewport().arvr = true
			OS.vsync_enabled = false # Causes performance problem in-headset
			headset_type = HEADSET_TYPE.RIFT
	else: # Attempt loading Quest
		var ovr_init_config_plugin = preload("res://addons/godot_ovrmobile/OvrInitConfig.gdns")
		var ovr_performance_plugin = preload("res://addons/godot_ovrmobile/OvrPerformance.gdns")
		
		if ovr_init_config_plugin and ovr_performance_plugin:
			ovr_init_config = ovr_init_config_plugin.new()
			ovr_performance = ovr_performance_plugin.new()
	
			interface = ARVRServer.find_interface("OVRMobile")
			if interface:				
				ovr_init_config.set_render_target_size_multiplier(1)

				if interface.initialize():
					var ovr_proxy = preload("res://addons/godot_ovrmobile/OvrVrApiProxy.gdns").new();
					var ovr_types = preload("res://addons/godot_ovrmobile/OvrVrApiTypes.gd").new();					
					dev_type = str(ovr_proxy.vrapi_get_system_property_int(ovr_types.OvrSystemProperty.VRAPI_SYS_PROP_DEVICE_TYPE))
					
					get_viewport().arvr = true
					match (dev_type):
						ovr_types.OvrDeviceType.VRAPI_DEVICE_TYPE_OCULUSQUEST: headset_type = HEADSET_TYPE.QUEST1
						ovr_types.OvrDeviceType.VRAPI_DEVICE_TYPE_OCULUSQUEST2: headset_type = HEADSET_TYPE.QUEST2
						_: headset_type = HEADSET_TYPE.QUEST1


func _process(_delta):
	if not perform_runtime_config:
		if headset_type == HEADSET_TYPE.QUEST1 || headset_type == HEADSET_TYPE.QUEST2:
			ovr_performance.set_clock_levels(1, 1)
			ovr_performance.set_extra_latency_mode(1)
			var ovr_display = preload("res://addons/godot_ovrmobile/OvrDisplay.gdns").new()
			var rates = ovr_display.get_supported_display_refresh_rates()
			var target = rates.max()
			ovr_display.set_display_refresh_rate(target)
			refresh_rate = target
		elif headset_type == HEADSET_TYPE.RIFT:
			if refresh_rate == 0:
				refresh_rate = round(rift_config.get_refresh_rate())
		
		Engine.iterations_per_second = refresh_rate
		perform_runtime_config = true
